(function ($) {

  $('#btnLoadText').click(function () { $("#showResult").load("show.txt"); });
  $('#btnAjax').click(function () { callRestAPI() });
  $('#btnAjax1').click(function () { callRestAPI1() });

  // Perform an asynchronous HTTP (Ajax) API request.
  function callRestAPI() {
    var root = 'https://jsonplaceholder.typicode.com';
    $.ajax({
      url: root + '/users/1',
      method: 'GET'
    }).then(function (response) {
      console.log(response);
      $('#showResult').html(response.phone);
    });
  }

function callRestAPI1() {
    var root = 'https://jsonplaceholder.typicode.com';
    $.ajax({
      url: root + '/comments/1',
      method: 'GET'
    }).then(function (response) {
      console.log(response);
      $('#showResult').html(response.body);
    });
  }
})($);
